import models.Circle;
import models.Rectangle;
import models.Square;

public class App {
    public static void main(String[] args) throws Exception {
        Rectangle rectangle1 = new Rectangle(5.1, 3.0);
        Circle circle1 = new Circle(3);
        Square square = new Square(5);

        System.out.println(rectangle1.getArea());
        System.out.println(circle1.getArea());
        System.out.println(square.getArea());

        System.out.println(rectangle1.getPerimeter());
        System.out.println(circle1.getPerimeter());
        System.out.println(square.getPerimeter());

        System.out.println(rectangle1.toString());
        System.out.println(circle1.toString());
        System.out.println(square.toString());

    }
}
