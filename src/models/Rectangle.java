package models;

public class Rectangle extends models.Shape {

    private double width = 1.0;
    private double height = 1.0;

    

    public Rectangle() {
    }

    

    public Rectangle(double width, double height) {
        this.width = width;
        this.height = height;
    }



    public Rectangle(String color, boolean filled, double width, double height) {
        super(color, filled);
        this.width = width;
        this.height = height;
    }



    public Rectangle(String color, boolean filled) {
    }



    public double getWidth() {
        return width;
    }



    public void setWidth(double width) {
        this.width = width;
    }



    public double getHeight() {
        return height;
    }



    public void setHeight(double height) {
        this.height = height;
    }



    @Override
    public
    double getArea() {
        // TODO Auto-generated method stub
       return this.width*this.height;
    }

    @Override
    public
    double getPerimeter() {
        // TODO Auto-generated method stub
       return 2*(this.width+this.height);
    }



    @Override
    public String toString() {
        return "Rectangle ["+super.toString()+"width=" + width + ", height=" + height + "]";
    }

}
