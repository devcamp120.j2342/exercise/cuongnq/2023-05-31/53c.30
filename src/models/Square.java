package models;

public class Square extends models.Rectangle {
    private double side = 1.0;

    public Square() {
    }

    public Square(double side) {
        this.side = side;
    }

    public Square(String color, boolean filled, double side) {
        super(color, filled);
        this.side = side;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    @Override
    public String toString() {
        return "Square ["+super.toString()+"side=" + side + "]";
    }

    
    @Override
    public
    double getArea() {
        // TODO Auto-generated method stub
       return this.side*this.side;
    }

    @Override
    public
    double getPerimeter() {
        // TODO Auto-generated method stub
       return 2*(this.side+this.side);
    }

}
