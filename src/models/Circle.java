package models;

public class Circle extends models.Shape{
    private double radius = 1.0;

    public Circle() {
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle(String color, boolean filled, double radius) {
        super(color, filled);
        this.radius = radius;
    }

    @Override
    public
    double getArea() {
        // TODO Auto-generated method stub
        return this.radius*this.radius*Math.PI;
    }

    @Override
    public
    double getPerimeter() {
        // TODO Auto-generated method stub
        return 2*Math.PI*this.radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public String toString() {
        return "Circle ["+ super.toString() +"radius=" + radius + "]";
    }

    
  
    
}
